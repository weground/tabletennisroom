#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>

void setup() {
  Serial.begin(115200);
}

void loop() {

  String data = "";
  
  while (Serial.available()){
    data=Serial.readString();
  }

  if (data.equals("motion") or data.equals("empty")){
    sendStatusToServer(data.equals("motion"));
  }
}

void sendStatusToServer(bool isMotion) {
  
  const char* ssid     = "<wifi_network_name>";
  const char* password = "<wifi_netowrk_password>";
  const int httpPort = 80;
  const char* host = "table-tennis-room.herokuapp.com";

  String url = isMotion ? "/isMotion" : "/isEmpty";

  WiFiClient client;
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  
  if (!client.connect(host, httpPort)) {
    Serial.println("not connected");
    return;
  }
  
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");
  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      client.stop();
      Serial.println("timeout");
      return;
    }
  }

  delay(500);
}
