#include <SoftwareSerial.h>

const int MOTION_SENSOR_PIN = 7;
const int MOTION_TRIGGER = 20;
const int EMPTY_TRIGGER = 50;
const int SENSOR_HISTORY_SIZE = 60;

int motionCounter;
int emptyCounter;
int sensorHistory[SENSOR_HISTORY_SIZE];
int historyIndex;
bool isMotion;
bool isRunning;

void setup() {
  Serial.begin(115200);
  
  changeMotionStatus(false);
}

void loop() {
  if(!isRunning) {
    isRunning = true;
    int sensorValue = digitalRead(MOTION_SENSOR_PIN);
    sensorHistory[historyIndex] = sensorValue;
 
    if(isMotionDetected(sensorValue)) {
      if(!isMotionAlert()) {
        motionCounter++;
      } else if(!isMotion) {
        resetCounters();
        if(isMotionAlert()) {
          changeMotionStatus(true);
        }
      }
    } else {
      if(!isEmptyAlert()) {
        emptyCounter++;
      } else if(isMotion) {
        resetCounters();
        if(isEmptyAlert()) {
          changeMotionStatus(false);
        }
      }
    }
    
    historyIndex = (historyIndex == SENSOR_HISTORY_SIZE-1) ? 0 : historyIndex+1;

    isRunning = false;
  }
  delay(1000); 
}

void resetCounters() {
  int motionTempCounter = 0;
  int emptyTempCounter = 0;
  for (int i = 0; i < SENSOR_HISTORY_SIZE; i++) {
    if(sensorHistory[i] == 1) {
      motionTempCounter++;
    } else {
      emptyTempCounter++;
    }
  }
  motionCounter = motionTempCounter;
  emptyCounter = emptyTempCounter;
}

bool isMotionAlert() {
  return motionCounter >= MOTION_TRIGGER;
}

bool isEmptyAlert() {
  return emptyCounter >= EMPTY_TRIGGER;
}

bool isMotionDetected(int sensorValue) {
  return sensorValue == 1;
}

void changeMotionStatus(bool motionStatus) {
  isMotion = motionStatus;
  motionCounter = 0;
  emptyCounter = 0;
  sendInfoToESP();
}

void sendInfoToESP()
{
  Serial.write(isMotion ? "motion" : "empty");
}
