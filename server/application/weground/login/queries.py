from datetime import datetime

from sqlalchemy import and_

from application.db import db
from application.models import Company
from application.models import User


class Queries(object):
    def __init__(self):
        pass

    @staticmethod
    def add_user(fb_id, first_name, last_name, gender, profile_pic, locale, timezone):
        try:
            user = User(
                fb_id=fb_id,
                first_name=first_name,
                last_name=last_name,
                gender=gender,
                profile_pic=profile_pic,
                locale=locale[:2],
                timezone=timezone
            )
            db.session.add(user)
            db.session.flush()
            db.session.commit()
            print("New user added to database")

            return user
        except:
            db.session.rollback()
            print("Unable to add user to database.")
            raise

    @staticmethod
    def get_admin_of_company_id(company_id):
        user = User.query.filter(and_(User.company_id == company_id, User.is_admin == True)).first()
        return user

    @staticmethod
    def update_user_company(user, company_id):
        db.session.query(User).filter(User.id == user.id).update({'company_id': company_id})
        db.session.commit()
        return None

    @staticmethod
    def update_user_active(user, is_active):
        db.session.query(User).filter(User.id == user.id).update({'is_active': is_active})
        db.session.commit()
        return None

    @staticmethod
    def update_last_activity_of_user(user):
        user.last_activity_at = datetime.utcnow()
        db.session.commit()
        return None

    @staticmethod
    def get_company_by_id(company_id):
        company = Company.query.filter_by(id=company_id).first()
        return company

    @staticmethod
    def get_company_list():
        # companies = db.session.query(Company).order_by(Company.name).all()
        companies = db.session.query(Company).filter(Company.id == 1).order_by(Company.name).all()  # Stefanini only
        return companies
