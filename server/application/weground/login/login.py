import os

import requests

import application.fbmessenger.fbmessage
from application.fbmessenger.fbmessage import FbMessage
from .constants import *
from .queries import Queries
from ..base import Base
from ..base import Element
from ..constants import *

DEFAULT_API_VERSION = 2.7


class Login(Base):
    def __init__(self):
        Base.__init__(self)
        self.queries = Queries()

    def user_get_started(self, fb_id):
        user_profile = self.get_fb_user_profile(fb_id)

        messages = []

        user = self.database.get_user_by_fb_id(fb_id)

        if user:
            if user.company_id is not None:
                company = self.queries.get_company_by_id(user.company_id)

                custom_message = self.database.get_custom_message(CM_KEY_LOGIN_WELCOME_BACK_MESSAGE, user).format(user.first_name, company.name)
                if custom_message:
                    messages.append(self.create_text_message(custom_message, user.fb_id))

                messages = messages + self.show_tutorial(user.fb_id)
        else:
            user = self.queries.add_user(fb_id, user_profile['first_name'], user_profile['last_name'], user_profile['gender'], user_profile['profile_pic'], user_profile['locale'], user_profile['timezone'])

        if user.company_id is None:
            messages = messages + self.ask_company(user)

        self.database.add_text_message(user.id, None, "GetStarted")
        self.queries.update_last_activity_of_user(user)

        self.log_messages_in_database(user, messages)

        return messages

    @staticmethod
    def get_fb_user_profile(sender_id):
        # get fb user profile
        user_profile_url = (
            "https://graph.facebook.com"
            "/v{0}/{1}?access_token={2}"
            "&fields=first_name,last_name,profile_pic,locale,timezone,gender"
        ).format(DEFAULT_API_VERSION, sender_id, os.environ["PAGE_ACCESS_TOKEN"])

        payload = {}
        user_profile = requests.get(user_profile_url, json=payload).json()

        return user_profile

    def ask_company(self, user):
        messages = []

        custom_message = self.database.get_custom_message(CM_KEY_LOGIN_ASK_FOR_COMPANY, user)
        if custom_message:
            messages.append(self.create_text_message(custom_message, user.fb_id))

        company_list = self.queries.get_company_list()

        elements = []
        for company in company_list:

            buttons = [{
                "title": self.database.get_custom_message(CM_KEY_LOGIN_JOIN_TO_COMPANY, user),
                "type": "postback",
                "payload": "{}:{}:{}".format(PAYLOAD_LOGIN, PAYLOAD_LOGIN_JOIN_TO_COMPANY, company.id)
            }]

            title = company.name
            subtitle = ""
            image_url = "https://{}.herokuapp.com/static/logo/{}".format(os.environ["APP_NAME"], company.logo_url)

            element = Element(title=title, image_url=image_url, subtitle=subtitle, buttons=buttons)
            elements.append(element)

        fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_GENERIC_MESSAGE, user.fb_id)
        fb_message.elements = elements
        messages.append(fb_message)

        return messages

    def login_join_to_company(self, sender_id, company_id):
        messages = []

        user = self.database.get_user_by_fb_id(sender_id)
        company = self.queries.get_company_by_id(company_id)

        if company.user_need_approval:
            admin = self.queries.get_admin_of_company_id(company_id)

            messages = messages + self.send_approve_request(admin, user, company)

            custom_message = self.database.get_custom_message(CM_KEY_LOGIN_JOIN_REQUEST_SENT, user)
            messages.append(self.create_text_message(custom_message, user.fb_id))
        else:
            self.queries.update_user_company(user, company_id)
            self.queries.update_user_active(user, True)
            messages = messages + self.show_tutorial(user.fb_id)

        return messages

    def send_approve_request(self, admin, user, company):
        messages = []

        quick_replies = [{
            "content_type": "text",
            "title": self.database.get_custom_message(CM_KEY_REP_APPROVE, admin),
            "payload": "{}:{}:{}:{}".format(PAYLOAD_LOGIN, PAYLOAD_LOGIN_ASK_APPROVAL, user.id, "1")
        }, {
            "content_type": "text",
            "title": self.database.get_custom_message(CM_KEY_REP_DECLINE, admin),
            "payload": "{}:{}:{}:{}".format(PAYLOAD_LOGIN, PAYLOAD_LOGIN_ASK_APPROVAL, user.id, "0")
        }]

        fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_QUICK_REPLIES_MESSAGE, admin.fb_id)
        fb_message.text_message = self.database.get_custom_message(CM_KEY_LOGIN_JOIN_APPROVAL_REQUEST, admin).format(user.first_name, user.last_name, company.name)
        fb_message.quick_replies = quick_replies

        messages.append(fb_message)

        return messages

    def login_receive_approve_request(self, sender_id, user_id, answer):
        messages = []

        new_user = self.database.get_user_by_id(user_id)
        user = self.database.get_user_by_fb_id(sender_id)

        if answer == "1":
            self.queries.update_user_company(new_user, user.company_id)
            self.queries.update_user_active(new_user, True)

            custom_message = self.database.get_custom_message(CM_KEY_LOGIN_APPROVED_JOIN_REQUEST, new_user)
            messages.append(self.create_text_message(custom_message, new_user.fb_id))

            messages = messages + self.show_tutorial(new_user.fb_id)
        else:
            custom_message = self.database.get_custom_message(CM_KEY_LOGIN_DECLINED_JOIN_REQUEST, new_user)
            messages.append(self.create_text_message(custom_message, new_user.fb_id))

        return messages
