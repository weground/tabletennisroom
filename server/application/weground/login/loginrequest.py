from .login import Login
from .constants import *


class LoginRequest(object):
    def __init__(self):
        pass

    @staticmethod
    def process_request(fb_id, payload):
        messages = []
        login = Login()

        if payload.startswith(PAYLOAD_LOGIN_JOIN_TO_COMPANY):
            company_id = int(payload.split(':')[1])
            messages = login.login_join_to_company(fb_id, company_id)

        elif payload.startswith(PAYLOAD_LOGIN_ASK_APPROVAL):
            user_id = int(payload.split(':')[1])
            answer = payload.split(':')[2]
            messages = login.login_receive_approve_request(fb_id, user_id, answer)

        return messages
