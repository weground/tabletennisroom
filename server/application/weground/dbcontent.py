import os

import xlrd
import xlwt

try:
    # For Python 3.0 and later
    from urllib.request import urlopen
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen

from application.database import Database


class DbContent(object):
    def __init__(self):
        self.database = Database()
        pass

    def get_translations(self):

        style0 = xlwt.easyxf('font: name Arial, color-index blue, bold on', num_format_str='#,##0.00')
        style1 = xlwt.easyxf('font: name Arial, color-index black, bold off')

        wb = xlwt.Workbook()
        ws = wb.add_sheet('CustomMessage table')

        ws.write(0, 0, "id", style0)
        ws.write(0, 1, "key", style0)
        ws.write(0, 2, "language", style0)
        ws.write(0, 3, "message", style0)
        ws.write(0, 4, "message_female", style0)
        ws.write(0, 5, "message_male", style0)

        translations = self.database.get_custom_messages()

        for i in range(0, len(translations)):
            custom_message = translations[i]
            ws.write(i+1, 0, custom_message.id, style1)
            ws.write(i+1, 1, custom_message.key, style1)
            ws.write(i+1, 2, custom_message.language, style1)
            ws.write(i+1, 3, custom_message.message, style1)
            ws.write(i+1, 4, custom_message.message_female, style1)
            ws.write(i+1, 5, custom_message.message_male, style1)

        file_path = "{}/CustomMessage.xls".format(os.getcwd())

        wb.save(file_path)

        return file_path

    def set_translations(self, file_path):
        socket = urlopen(file_path)

        book = xlrd.open_workbook(file_contents=socket.read())

        sh = book.sheet_by_index(0)

        self.database.delete_custom_messages()

        for i in range(1, sh.nrows):
            key = sh.cell_value(rowx=i, colx=1)
            language = sh.cell_value(rowx=i, colx=2)
            message = sh.cell_value(rowx=i, colx=3)
            message_female = sh.cell_value(rowx=i, colx=4)
            message_male = sh.cell_value(rowx=i, colx=5)

            self.database.add_custom_message(key, language, message, message_female, message_male)

        return None
