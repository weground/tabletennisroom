import application.fbmessenger.fbmessage
from application.fbmessenger.fbmessage import FbMessage
from .constants import *
from .queries import Queries
from ..base import Base
from ..constants import *


class PingPong(Base):
    def __init__(self):
        Base.__init__(self)
        self.queries = Queries()

    def process(self, user):
        messages = []

        request = self.queries.get_ping_pong_request_of_user(user, True)

        ping_pong_room = self.queries.get_ping_pong_room()
        if ping_pong_room is None or ping_pong_room.is_empty_room:

            custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_ROOM_IS_EMPTY, user).format(ping_pong_room.created_at.strftime("%d %b, %H:%M"))
            messages.append(self.create_text_message(custom_message, user.fb_id))

            if request is not None:
                messages.append(self.ask_for_keep_alive(user))

        else:

            if request is not None:
                requests_count = self.queries.get_ping_pong_requests_count(request)
                if requests_count == 0:
                    custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_VALID_REQUEST_NO_USERS, user)
                elif requests_count == 1:
                    custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_VALID_REQUEST_ONE_USER, user)
                else:
                    custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_VALID_REQUEST_MORE_USERS, user).format(requests_count)

                messages.append(self.create_text_message(custom_message, user.fb_id))

                messages.append(self.ask_for_keep_alive(user))

            else:
                requests_count = self.queries.get_all_ping_pong_requests_count()
                if requests_count == 0:
                    message = self.database.get_custom_message(CM_KEY_PING_PONG_CREATE_REQUEST_NO_USERS, user)
                elif requests_count == 1:
                    message = self.database.get_custom_message(CM_KEY_PING_PONG_CREATE_REQUEST_ONE_USER, user)
                else:
                    message = self.database.get_custom_message(CM_KEY_PING_PONG_CREATE_REQUEST_MORE_USERS, user).format(requests_count)
                
                message = "{} {}".format(message, self.database.get_custom_message(CM_KEY_PING_PONG_CREATE_REQUEST_QUESTION, user).format(requests_count))

                quick_replies = [{
                    "content_type": "text",
                    "title": self.database.get_custom_message(CM_KEY_REP_YES, user),
                    "payload": "{}:{}:{}".format(PAYLOAD_PING_PONG, PAYLOAD_PING_PONG_CREATE_REQUEST, 1)
                }, {
                    "content_type": "text",
                    "title": self.database.get_custom_message(CM_KEY_REP_NO, user),
                    "payload": "{}:{}:{}".format(PAYLOAD_PING_PONG, PAYLOAD_PING_PONG_CREATE_REQUEST, 0)
                }]

                fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_QUICK_REPLIES_MESSAGE, user.fb_id)
                fb_message.text_message = message
                fb_message.quick_replies = quick_replies

                messages.append(fb_message)

        return messages

    # ask user if he still wants to keep alive his existing the request
    def ask_for_keep_alive(self, user):

        quick_replies = [{
            "content_type": "text",
            "title": self.database.get_custom_message(CM_KEY_REP_YES, user),
            "payload": "{}:{}:{}".format(PAYLOAD_PING_PONG, PAYLOAD_PING_PONG_REQUEST_KEEP_ALIVE, 1)
        }, {
            "content_type": "text",
            "title": self.database.get_custom_message(CM_KEY_REP_NO, user),
            "payload": "{}:{}:{}".format(PAYLOAD_PING_PONG, PAYLOAD_PING_PONG_REQUEST_KEEP_ALIVE, 0)
        }]

        fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_QUICK_REPLIES_MESSAGE, user.fb_id)
        fb_message.text_message = self.database.get_custom_message(CM_KEY_PING_PONG_REQUEST_KEEP_ALIVE, user)
        fb_message.quick_replies = quick_replies

        return fb_message

    def ping_pong_create_request(self, fb_id, is_create):
        user = self.database.get_user_by_fb_id(fb_id)

        messages = []

        if is_create:
            self.queries.add_ping_pong_request(user.id, True)

            custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_CREATED_REQUEST, user).format(user.first_name)
            messages.append(self.create_text_message(custom_message, user.fb_id))

        else:
            messages = messages + self.show_tutorial(user.fb_id)

        return messages

    def ping_pong_keep_alive(self, fb_id, is_keep_alive):
        user = self.database.get_user_by_fb_id(fb_id)

        messages = []

        if is_keep_alive:
            custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_KEEP_ALIVE_YES, user).format(user.first_name)
            messages.append(self.create_text_message(custom_message, user.fb_id))

        else:
            self.queries.invalidate_ping_pong_request(user)

            custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_KEEP_ALIVE_NO, user)
            messages.append(self.create_text_message(custom_message, user.fb_id))

        return messages

    def ping_pong_toggle(self, is_empty_room):
        messages = []

        current_room_status = self.queries.get_ping_pong_room_status()

        if current_room_status is None or current_room_status != is_empty_room:
            self.queries.set_ping_pong_room_status(is_empty_room)

            if is_empty_room:
                messages = messages + self.process_next_request()

        return messages

    def process_next_request(self):
        messages = []

        ping_pong = self.queries.get_next_ping_pong_request()
        if ping_pong is not None:
            user = self.database.get_user_by_id(ping_pong.user_id)

            quick_replies = [{
                "content_type": "text",
                "title": self.database.get_custom_message(CM_KEY_REP_YES, user),
                "payload": "{}:{}:{}".format(PAYLOAD_PING_PONG, PAYLOAD_PING_PONG_PLAY, 1)
            }, {
                "content_type": "text",
                "title": self.database.get_custom_message(CM_KEY_REP_NO, user),
                "payload": "{}:{}:{}".format(PAYLOAD_PING_PONG, PAYLOAD_PING_PONG_PLAY, 0)
            }]

            fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_QUICK_REPLIES_MESSAGE, user.fb_id)
            fb_message.text_message = self.database.get_custom_message(CM_KEY_PING_PONG_YOUR_TURN, user)
            fb_message.quick_replies = quick_replies

            messages.append(fb_message)

        return messages

    def ping_pong_play(self, fb_id, is_play):
        user = self.database.get_user_by_fb_id(fb_id)

        messages = []

        self.queries.invalidate_ping_pong_request(user)

        if is_play:
            custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_USER_PLAY, user).format(user.first_name)
            messages.append(self.create_text_message(custom_message, user.fb_id))

        else:
            custom_message = self.database.get_custom_message(CM_KEY_PING_PONG_USER_NO_PLAY, user)
            messages.append(self.create_text_message(custom_message, user.fb_id))

            messages = messages + self.process_next_request()

            messages = messages + self.show_tutorial(user.fb_id)

        return messages
