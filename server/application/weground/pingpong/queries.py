from datetime import datetime, timedelta

from sqlalchemy import and_, or_, desc, func

from application.db import db
from application.models import PingpongRequests
from application.models import PingpongRoom


class Queries(object):
    def __init__(self):
        pass

    @staticmethod
    def add_ping_pong_request(user_id, is_looking_for_empty_room):
        try:
            ping_pong = PingpongRequests(
                user_id=user_id,
                is_processed=False,
                is_looking_for_empty_room = is_looking_for_empty_room
            )
            db.session.add(ping_pong)
            db.session.flush()
            db.session.commit()
            print("New ping_pong added to database")

            return ping_pong
        except:
            db.session.rollback()
            print("Unable to add ping_pong to database.")
            raise

    @staticmethod
    def get_ping_pong_room():
        room = db.session.query(PingpongRoom).order_by(desc(PingpongRoom.id)).first()
        return room

    @staticmethod
    def get_ping_pong_request_of_user(user, is_looking_for_empty_room):
        request = db.session.query(PingpongRequests).\
            filter(and_(PingpongRequests.user_id == user.id,
                        PingpongRequests.is_processed == False,
                        PingpongRequests.is_looking_for_empty_room == is_looking_for_empty_room,
                        PingpongRequests.created_at >= Queries.get_today())).\
            order_by(desc(PingpongRequests.id)).\
            first()

        return request

    @staticmethod
    def get_ping_pong_requests_count(request):
        return db.session.query(func.count(PingpongRequests.id)).\
            filter(and_(PingpongRequests.is_processed == False,
                        PingpongRequests.is_looking_for_empty_room == True,
                       PingpongRequests.created_at >= Queries.get_today(),
                       PingpongRequests.id < request.id)).\
            scalar()

    @staticmethod
    def get_all_ping_pong_requests_count():
        return db.session.query(func.count(PingpongRequests.id)). \
            filter(and_(PingpongRequests.is_processed == False,
                        PingpongRequests.is_looking_for_empty_room == True,
                       PingpongRequests.created_at >= Queries.get_today())). \
            scalar()

    @staticmethod
    def invalidate_ping_pong_request(user):
        db.session.query(PingpongRequests).\
            filter(and_(PingpongRequests.user_id == user.id,
                        PingpongRequests.is_processed == False,
                        PingpongRequests.created_at >= Queries.get_today())).\
            update({'is_processed': True})
        db.session.commit()
        return None

    @staticmethod
    def get_next_ping_pong_request():
        request = db.session.query(PingpongRequests). \
            filter(and_(PingpongRequests.is_processed == False,
                        PingpongRequests.is_looking_for_empty_room == True,
                        PingpongRequests.created_at >= Queries.get_today())). \
                        order_by(desc(PingpongRequests.id)).first()
        return request

    @staticmethod
    def set_ping_pong_room_status(is_empty_room):
        try:
            ping_pong_room = PingpongRoom(
                is_empty_room=is_empty_room
            )
            db.session.add(ping_pong_room)
            db.session.flush()
            db.session.commit()
            print("New ping_pong_room added to database")

            return ping_pong_room
        except:
            db.session.rollback()
            print("Unable to add ping_pong_room to database.")
            raise

    @staticmethod
    def get_ping_pong_room_status():
        record = db.session.query(PingpongRoom).\
            order_by(desc(PingpongRoom.id)).\
            first()
        if record is not None:
            return record.is_empty_room
        else:
            return None

    @staticmethod
    def get_today():
        return datetime.utcnow() - timedelta(days=1)
