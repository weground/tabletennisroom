from .pingpong import PingPong
from .constants import *


class PingPongRequest(object):
    def __init__(self):
        pass

    @staticmethod
    def process_request(fb_id, payload):

        messages = []
        ping_pong = PingPong()

        if payload.startswith(PAYLOAD_PING_PONG_CREATE_REQUEST):
            is_create = True if payload.split(':')[1] == "1" else False
            messages = ping_pong.ping_pong_create_request(fb_id, is_create)

        elif payload.startswith(PAYLOAD_PING_PONG_REQUEST_KEEP_ALIVE):
            is_keep_alive = True if payload.split(':')[1] == "1" else False
            messages = ping_pong.ping_pong_keep_alive(fb_id, is_keep_alive)

        elif payload.startswith(PAYLOAD_PING_PONG_TOGGLE):
            is_empty_room = True if payload.split(':')[1] == "1" else False
            messages = ping_pong.ping_pong_toggle(is_empty_room)

        elif payload.startswith(PAYLOAD_PING_PONG_PLAY):
            is_play = True if payload.split(':')[1] == "1" else False
            messages = ping_pong.ping_pong_play(fb_id, is_play)

        return messages
