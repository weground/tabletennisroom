PAYLOAD_PING_PONG = "payload_ping_pong"
PAYLOAD_PING_PONG_REQUEST_KEEP_ALIVE = "keep_alive"
PAYLOAD_PING_PONG_CREATE_REQUEST = "create_request"
PAYLOAD_PING_PONG_TOGGLE = "toggle"
PAYLOAD_PING_PONG_PLAY = "play"

CM_KEY_PING_PONG_ROOM_IS_EMPTY = "PING_PONG_ROOM_IS_EMPTY"
CM_KEY_PING_PONG_ROOM_IS_NOT_EMPTY = "PING_PONG_ROOM_IS_NOT_EMPTY"
CM_KEY_PING_PONG_VALID_REQUEST_NO_USERS = "PING_PONG_VALID_REQUEST_NO_USERS"
CM_KEY_PING_PONG_VALID_REQUEST_ONE_USER = "PING_PONG_VALID_REQUEST_ONE_USER"
CM_KEY_PING_PONG_VALID_REQUEST_MORE_USERS = "PING_PONG_VALID_REQUEST_MORE_USERS"
CM_KEY_PING_PONG_REQUEST_KEEP_ALIVE = "PING_PONG_REQUEST_KEEP_ALIVE"
CM_KEY_PING_PONG_KEEP_ALIVE_YES = "PING_PONG_KEEP_ALIVE_YES"
CM_KEY_PING_PONG_KEEP_ALIVE_NO = "PING_PONG_KEEP_ALIVE_NO"
CM_KEY_PING_PONG_CREATE_REQUEST_NO_USERS = "PING_PONG_CREATE_REQUEST_NO_USERS"
CM_KEY_PING_PONG_CREATE_REQUEST_ONE_USER = "PING_PONG_CREATE_REQUEST_ONE_USER"
CM_KEY_PING_PONG_CREATE_REQUEST_MORE_USERS = "PING_PONG_CREATE_REQUEST_MORE_USERS"
CM_KEY_PING_PONG_CREATE_REQUEST_QUESTION = "PING_PONG_CREATE_REQUEST_QUESTION"
CM_KEY_PING_PONG_CREATED_REQUEST = "PING_PONG_CREATED_REQUEST"
CM_KEY_PING_PONG_YOUR_TURN = "PING_PONG_YOUR_TURN"
CM_KEY_PING_PONG_USER_PLAY = "PING_PONG_USER_PLAY"
CM_KEY_PING_PONG_USER_NO_PLAY = "PING_PONG_USER_NO_PLAY"
