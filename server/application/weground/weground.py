import wolframalpha

import application.fbmessenger.fbmessage
from application.fbmessenger.fbmessage import FbMessage
from .base import Base
from .constants import *
from .dbcontent import DbContent
from .pingpong.pingpong import PingPong


def create_text_message(text_message, destination_fb_id):
    fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_TEXT_MESSAGE, destination_fb_id)
    fb_message.text_message = text_message
    return fb_message


class Weground(Base):
    def __init__(self):
        Base.__init__(self)
        self.last_receiver_id = 0  # when a new message is sent to an user, this variable keeps the id of that user

    def open_section(self, sender_id, section_index, extra):
        messages = []

        user = self.database.get_user_by_fb_id(sender_id)
        self.database.update_user_status(user, section_index, 1)

        messages = messages + self.process_section(sender_id, section_index, extra)

        return messages

    def process_section(self, sender_id, section_index, extra):
        messages = []

        user = self.database.get_user_by_fb_id(sender_id)

        if section_index == SECTION_PING_PONG_INDEX:
            ping_pong = PingPong()
            messages = messages + ping_pong.process(user)

        return messages

    # implement the Bot logic
    def process_user_message(self, sender_id, text_message):

        messages = []

        # get the current user based on the fb_id of the user which sent the fb message
        user = self.database.get_user_by_fb_id(sender_id)

        if not user:
            print("big error - not existing user")
            return []

        
        # IA side
        client = wolframalpha.Client("3J2KHE-2KQA6EQPW2")
        res = client.query(text_message)

        try:
            message = next(res.results).text
            messages.append(self.create_text_message(message, user.fb_id))
        except:
            custom_message = self.database.get_custom_message(CM_KEY_FALLBACK_MESSAGE, user).format(text_message)
            if custom_message:
                messages.append(self.create_text_message(custom_message, user.fb_id))

        self.log_messages_in_database(user, messages)

        return messages

    def process_user_file(self, sender_id, text_message, file_path):

        messages = []

        user = self.database.get_user_by_fb_id(sender_id)

        if not user:
            print("big error - not existing user")
            return []

        if self.is_command(text_message, user):
            print("is_command")
            if self.is_set_translations_command(text_message, user):
                db_content = DbContent()
                db_content.set_translations(file_path)

                custom_message = self.database.get_custom_message(CM_KEY_CUSTOM_MESSAGES_UPDATED, user)
                if custom_message:
                    messages.append(self.create_text_message(custom_message, user.fb_id))

        return messages

    def is_command(self, text_message, user):
        result = False

        if self.is_get_translations_command(text_message, user):
            result = True
        elif self.is_set_translations_command(text_message, user):
            result = True

        return result

    def is_get_translations_command(self, text_message, user):
        return user.is_admin and text_message == self.database.get_custom_message(CM_KEY_COMMAND_GET_TRANSLATIONS, user)

    def is_set_translations_command(self, text_message, user):
        return user.is_admin and text_message == self.database.get_custom_message(CM_KEY_COMMAND_SET_TRANSLATIONS, user)

    @staticmethod
    def create_file_message(file_path, destination_fb_id):
        fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_FILE_MESSAGE, destination_fb_id)
        fb_message.file_path = file_path
        return fb_message
