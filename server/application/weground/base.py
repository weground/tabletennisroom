import json
import os

import application.fbmessenger.fbmessage
from application.database import Database
from application.fbmessenger.fbmessage import FbMessage
from .constants import *


class Element(dict):
    __acceptable_keys = ['title', 'item_url', 'image_url', 'subtitle', 'buttons']

    def __init__(self, *args, **kwargs):
        kwargs = {k: v for k, v in kwargs.items() if k in self.__acceptable_keys}
        super(Element, self).__init__(*args, **kwargs)

    def to_json(self):
        return json.dumps({k: v for k, v in self.items() if k in self.__acceptable_keys})


class Base(object):
    def __init__(self):
        self.database = Database()

    @staticmethod
    def create_text_message(text_message, destination_fb_id):
        fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_TEXT_MESSAGE, destination_fb_id)
        fb_message.text_message = text_message
        return fb_message

    def leave_section(self, user):
        messages = []

        custom_message = self.database.get_custom_message(CM_KEY_LEAVE_SECTION, user)
        messages.append(self.create_text_message(custom_message, user.fb_id))

        messages = messages + self.show_tutorial(user.fb_id)

        return messages

    def show_tutorial(self, sender_id):
        messages = []

        user = self.database.get_user_by_fb_id(sender_id)

        tutorial_list = []
        if self.database.has_ping_pong_access(user):
            tutorial_list.append((SECTION_PING_PONG_INDEX, CM_KEY_SECTION_PING_PONG_TITLE, "https://{}.herokuapp.com/static/sections/ping_pong_section.png".format(os.environ["APP_NAME"])))

        if len(tutorial_list) > 0:
            custom_message = self.database.get_custom_message(CM_KEY_TUTORIAL_INFO, user).format(len(tutorial_list))
        else:
            custom_message = self.database.get_custom_message(CM_KEY_TUTORIAL_INFO_NO_SECTIONS, user)

        messages.append(self.create_text_message(custom_message, user.fb_id))

        elements = []
        for (element_index, element_title, picture_url) in tutorial_list:
            buttons = [{
                "title": self.database.get_custom_message(CM_KEY_OPEN_SECTION, user),
                "type": "postback",
                "payload": "{}:{}:1".format(PAYLOAD_SECTION, element_index)
            }]

            title = self.database.get_custom_message(element_title, user)
            subtitle = ""
            element = Element(title=title, image_url=picture_url, subtitle=subtitle, buttons=buttons)
            elements.append(element)

        fb_message = FbMessage(application.fbmessenger.fbmessage.TYPE_GENERIC_MESSAGE, user.fb_id)
        fb_message.elements = elements
        messages.append(fb_message)

        return messages

    def log_messages_in_database(self, user, messages):
        for fbMessage in messages:
            is_text = fbMessage.is_text_message()
            is_generic = fbMessage.is_generic_message()
            is_button = fbMessage.is_button_message()

            if fbMessage.destination_fb_id == user.fb_id:
                # it is about a message sent as response to the current user
                self.add_message_in_database(is_text, is_generic, is_button, None, user.id, fbMessage)

        return None

    def add_message_in_database(self, is_text, is_generic, is_button, from_id, to_id, fb_message):
        if is_text:
            self.database.add_text_message(from_id, to_id, fb_message.text_message)
        elif is_generic:
            self.database.add_generic_message(from_id, to_id, fb_message.elements)
        elif is_button:
            self.database.add_button_message(from_id, to_id, fb_message.buttons)

        return None
