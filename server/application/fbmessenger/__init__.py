import os

from flask import Flask
from application.db import db
from application.fbmessenger.fbprocess import process_data

app = Flask(__name__)


with app.app_context():
    app.config.from_object(os.environ['APP_SETTINGS'])
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.debug = True

    db.app = app
    db.init_app(app)
