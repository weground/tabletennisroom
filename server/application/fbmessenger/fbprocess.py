import os

from .fbbot import FbBot
from application.weground.constants import *
from application.weground.login.constants import *
from application.weground.login.login import Login
from application.weground.login.loginrequest import LoginRequest
from application.weground.pingpong.constants import *
from application.weground.pingpong.pingpongrequest import PingPongRequest
from application.weground.weground import Weground


def process_data(messaging_event):
    if messaging_event.get("postback"):
        payload = messaging_event["postback"]["payload"]
        sender_id = messaging_event["sender"]["id"]  # the facebook ID of the person sending you the message

        if payload == "START":  # new user clicked on "Get started"

            bot = Login()
            messages = bot.user_get_started(sender_id)

            send_response(messages)

        elif payload.startswith(PAYLOAD_LOGIN):
            messages = LoginRequest.process_request(sender_id, payload[len(PAYLOAD_LOGIN) + 1:])

            send_response(messages)

        elif payload.startswith(PAYLOAD_PING_PONG):
            messages = PingPongRequest.process_request(sender_id, payload[len(PAYLOAD_PING_PONG)+1:])

            send_response(messages)

        elif payload.startswith(PAYLOAD_TUTORIAL):

            bot = Weground()
            messages = bot.show_tutorial(sender_id)

            send_response(messages)

        elif payload.startswith(PAYLOAD_SECTION):

            args = payload.split(':')
            section_index = int(args[1])
            extra = None if len(args) == 2 else int(args[2])

            bot = Weground()
            messages = bot.open_section(sender_id, section_index, extra)

            send_response(messages)

        elif payload.startswith(PAYLOAD_NEXT_STEP):
            section_index = int(payload.split(':')[1])

            bot = Weground()
            messages = bot.process_section(sender_id, section_index, None)

            send_response(messages)

    elif messaging_event.get("message"):  # someone sent us a message
        print(messaging_event)
        sender_id = messaging_event["sender"]["id"]  # the facebook ID of the person sending you the message

        if messaging_event["message"].get("text"):
            text_message = messaging_event["message"]["text"]  # the message's text

            if messaging_event["message"].get("quick_reply"):
                payload = messaging_event["message"]["quick_reply"]["payload"]

                if payload.startswith(PAYLOAD_LOGIN):
                    messages = LoginRequest.process_request(sender_id, payload[len(PAYLOAD_LOGIN) + 1:])

                    send_response(messages)

                elif payload.startswith(PAYLOAD_PING_PONG):
                    messages = PingPongRequest.process_request(sender_id, payload[len(PAYLOAD_PING_PONG)+1:])

                    send_response(messages)

                elif payload.startswith(PAYLOAD_NEXT_STEP):
                    section_index = int(payload.split(':')[1])

                    bot = Weground()
                    messages = bot.process_section(sender_id, section_index, None)

                    send_response(messages)

            elif messaging_event["message"].get("attachments"):

                attachments = messaging_event["message"].get("attachments")
                if len(attachments) == 1:
                    attachment = attachments[0]
                    if attachment.get("type"):
                        if attachment["type"] == "file" or attachment["type"] == "image":
                            file_url = attachment["payload"]["url"]
                            print("file_url = {}".format(file_url))

                            bot = Weground()
                            messages = bot.process_user_file(sender_id, text_message, file_url)

                            send_response(messages)

            else:
                print(messaging_event)

                bot = Weground()
                messages = bot.process_user_message(sender_id, text_message)

                send_response(messages)

        else:
            print("this is strange:")
            print(messaging_event)

    if messaging_event.get("delivery"):  # delivery confirmation
        pass

    if messaging_event.get("optin"):  # optin confirmation
        pass

    return None


def send_response(messages):
    fb_bot = FbBot(os.environ["PAGE_ACCESS_TOKEN"], os.environ["PAGE_ID"])

    for fbMessage in messages:
        if fbMessage.is_text_message():
            fb_bot.send_text_message(fbMessage.destination_fb_id, fbMessage.text_message)
        elif fbMessage.is_generic_message():
            fb_bot.send_generic_message(fbMessage.destination_fb_id, fbMessage.elements)
        elif fbMessage.is_button_message():
            fb_bot.send_button_message(fbMessage.destination_fb_id, fbMessage.text_message, fbMessage.buttons)
        elif fbMessage.is_quick_replies_message():
            fb_bot.send_quick_replies_message(fbMessage.destination_fb_id, fbMessage.text_message, fbMessage.quick_replies)
        elif fbMessage.is_quick_replies_message_with_template():
            fb_bot.send_quick_replies_message_with_template(fbMessage.destination_fb_id, fbMessage.elements, fbMessage.quick_replies)
        elif fbMessage.is_file_message():
            fb_bot.send_file_message(fbMessage.destination_fb_id, fbMessage.file_path)

    return None
