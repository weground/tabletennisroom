import json
import requests
from requests_toolbelt import MultipartEncoder
from .utils import generate_appsecret_proof

DEFAULT_API_VERSION = 2.7


class FbBot(object):
    def __init__(self, access_token, page_id, api_version=DEFAULT_API_VERSION, app_secret=None):
        self.api_version = api_version
        self.access_token = access_token
        self.page_id = page_id
        self.base_url = (
            "https://graph.facebook.com"
            "/v{0}/me/messages?access_token={1}"
        ).format(self.api_version, access_token)

        if app_secret is not None:
            appsecret_proof = generate_appsecret_proof(access_token, app_secret)
            self.base_url += '&appsecret_proof={0}'.format(appsecret_proof)

    def configure_greeting_text(self, message):
        welcome_url = (
            "https://graph.facebook.com"
            "/v{0}/{1}/thread_settings?access_token={2}"
        ).format(self.api_version, self.page_id, self.access_token)

        payload = {
            'setting_type': 'greeting',
            'greeting': {
                "text": message
            }
        }

        result = requests.post(welcome_url, json=payload).json()
        print(result)

    def configure_get_started_button(self):

        welcome_url = (
            "https://graph.facebook.com"
            "/v{0}/me/thread_settings?access_token={1}"
        ).format(self.api_version, self.access_token)

        payload = {
            'setting_type': 'call_to_actions',
            'thread_state': 'new_thread',
            'call_to_actions': [
                {
                    "payload": 'START'
                }
            ]
        }

        result = requests.post(welcome_url, json=payload).json()
        print(result)

    def send_persistant_menu(self, buttons):
        welcome_url = (
            "https://graph.facebook.com"
            "/v{0}/{1}/thread_settings?access_token={2}"
        ).format(self.api_version, self.page_id, self.access_token)

        payload = {
            'setting_type': 'call_to_actions',
            'thread_state': 'existing_thread',
            'call_to_actions': buttons
        }

        result = requests.post(welcome_url, json=payload).json()
        print(result)

    def send_text_message(self, recipient_id, message):
        payload = {
            'recipient': {
                'id': recipient_id
            },
            'message': {
                'text': message
            }
        }
        return self._send_payload(payload)

    def send_message(self, recipient_id, message):
        payload = {
            'recipient': {
                'id': recipient_id
            },
            'message': message
        }
        return self._send_payload(payload)

    def send_quick_replies_message(self, recipient_id, text, quick_replies):
        payload = {
            'recipient': {
                'id': recipient_id
            },
            'message': {
                'text': text,
                'quick_replies': quick_replies
            }
        }
        return self._send_payload(payload)

    def send_quick_replies_message_with_template(self, recipient_id, elements, quick_replies):
        payload = {
            'recipient': {
                'id': recipient_id
            },
            'message': {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": elements
                    }
                },
                "quick_replies": quick_replies
            }
        }
        return self._send_payload(payload)

    def send_generic_message(self, recipient_id, elements):
        payload = {
            'recipient': {
                'id': recipient_id
            },
            'message': {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "generic",
                        "elements": elements
                    }
                }
            }
        }
        return self._send_payload(payload)

    def send_button_message(self, recipient_id, text, buttons):
        payload = {
            'recipient': {
                'id': recipient_id
            },
            'message': {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "button",
                        "text": text,
                        "buttons": buttons
                    }
                }
            }
        }
        return self._send_payload(payload)

    def _send_payload(self, payload):
        result = requests.post(self.base_url, json=payload).json()
        return result

    def send_image(self, recipient_id, image_path):
        """
            This sends an image to the specified recipient.
            Image must be PNG or JPEG.
            Input:
              recipient_id: recipient id to send to
              image_path: path to image to be sent
            Output:
              Response from API as <dict>
        """
        payload = {
            'recipient': json.dumps(
                {
                    'id': recipient_id
                }
            ),
            'message': json.dumps(
                {
                    'attachment': {
                        'type': 'image',
                        'payload': {}
                    }
                }
            ),
            'filedata': (image_path, open(image_path, 'rb'))
        }
        multipart_data = MultipartEncoder(payload)
        multipart_header = {
            'Content-Type': multipart_data.content_type
        }
        return requests.post(self.base_url, data=multipart_data, headers=multipart_header).json()

    def send_image_url(self, recipient_id, image_url):
        """
        Sends an image to specified recipient using URL.
            Image must be PNG or JPEG.
            Input:
              recipient_id: recipient id to send to
              image_url: url of image to be sent
            Output:
              Response from API as <dict>
        """
        payload = {
            'recipient': json.dumps(
                {
                    'id': recipient_id
                }
            ),
            'message': json.dumps(
                {
                    'attachment': {
                        'type': 'image',
                        'payload': {
                            'url': image_url
                        }
                    }
                }
            )
        }
        return self._send_payload(payload)

    def send_file_message(self, recipient_id, file_path):
        """
            This sends a file to the specified recipient.
            Input:
              recipient_id: recipient id to send to
              file_path: path to file to be sent
            Output:
              Response from API as <dict>
        """
        payload = {
            'recipient': json.dumps(
                {
                    'id': recipient_id
                }
            ),
            'message': json.dumps(
                {
                    'attachment': {
                        'type': 'file',
                        'payload': {}
                    }
                }
            ),
            'filedata': (file_path, open(file_path, 'rb'))
        }
        multipart_data = MultipartEncoder(payload)
        multipart_header = {
            'Content-Type': multipart_data.content_type
        }
        return requests.post(self.base_url, data=multipart_data, headers=multipart_header).json()
