TYPE_TEXT_MESSAGE = 0
TYPE_GENERIC_MESSAGE = 1
TYPE_BUTTON_MESSAGE = 2
TYPE_QUICK_REPLIES_MESSAGE = 3
TYPE_FILE_MESSAGE = 4


class FbMessage(object):
    def __init__(self, object_type, destination_fb_id):
        self.type = object_type
        self.destination_fb_id = destination_fb_id
        self.text_message = ''
        self.elements = []
        self.buttons = []
        self.quick_replies = []
        self.file_path = None
        pass

    def is_text_message(self):
        return self.type == TYPE_TEXT_MESSAGE

    def is_generic_message(self):
        return self.type == TYPE_GENERIC_MESSAGE

    def is_button_message(self):
        return self.type == TYPE_BUTTON_MESSAGE

    def is_quick_replies_message(self):
        return self.type == TYPE_QUICK_REPLIES_MESSAGE

    def is_file_message(self):
        return self.type == TYPE_FILE_MESSAGE
