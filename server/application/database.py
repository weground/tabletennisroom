from application.db import db
from .models import User
from .models import TextMessage
from .models import GenericMessage
from .models import ButtonMessage
from .models import Message
from .models import CustomMessage
from sqlalchemy import and_, or_, desc
from random import randint


class Database(object):
    def __init__(self):
        pass

    @staticmethod
    def get_user_by_fb_id(fb_id):
        user = User.query.filter_by(fb_id=fb_id).first()
        return user

    @staticmethod
    def get_user_by_id(user_id):
        user = User.query.filter_by(id=user_id).first()
        return user

    @staticmethod
    def get_users_list():
        users = db.session.query(User).all()
        return users

    @staticmethod
    def get_users_list_of_company(company_id):
        users = db.session.query(User).filter(User.company_id == company_id).all()
        return users

    @staticmethod
    def update_user_status(user, current_section, section_status):
        db.session.query(User).filter(User.id == user.id).update({'current_section': current_section, 'section_status': section_status})
        db.session.commit()
        return None

    @staticmethod
    def update_user_profile(fb_id, first_name, last_name, gender, profile_pic, locale, timezone):
        updated_user = {
            'first_name': first_name,
            'last_name': last_name,
            'gender': gender,
            'profile_pic': profile_pic,
            'locale': locale[:2],
            'timezone': timezone
        }
        db.session.query(User).filter(User.fb_id == fb_id).update(updated_user)
        db.session.commit()
        return None

    @staticmethod
    def reset_profile(fb_id):
        reset_user = {
            'first_name': None,
            'last_name': None,
            'gender': None,
            'profile_pic': None,
            'locale': None,
            'timezone': None,
            'last_profile_field_requested': None
        }
        db.session.query(User).filter(User.fb_id == fb_id).update(reset_user)
        db.session.commit()
        return None

    @staticmethod
    def add_text_message(from_id, to_id, message):
        try:
            text_message = TextMessage(
                message=message
            )
            db.session.add(text_message)
            db.session.flush()
            db.session.commit()
            print("New textMessage added to database")

            try:
                message = Message(
                    from_id=from_id,
                    to_id=to_id,
                    text_message_id=text_message.id,
                    generic_message_id=None,
                    button_message_id=None
                )
                db.session.add(message)
                db.session.flush()
                db.session.commit()
                print("New message added to database")

                return None
            except:
                db.session.rollback()
                print("Unable to add message to database.")
                return None

        except:
            db.session.rollback()
            print("Unable to add textMessage to database.")
            return None

    @staticmethod
    def add_generic_message(from_id, to_id, payload):
        try:
            generic_message = GenericMessage(
                payload=payload
            )
            db.session.add(generic_message)
            db.session.flush()
            db.session.commit()
            print("New genericMessage added to database")

            try:
                message = Message(
                    from_id=from_id,
                    to_id=to_id,
                    text_message_id=None,
                    generic_message_id=generic_message.id,
                    button_message_id=None
                )
                db.session.add(message)
                db.session.flush()
                db.session.commit()
                print("New message added to database")

                return None
            except:
                db.session.rollback()
                print("Unable to add message to database.")
                return None

        except:
            db.session.rollback()
            print("Unable to add genericMessage to database.")
            return None

    @staticmethod
    def add_button_message(from_id, to_id, payload):
        try:
            button_message = ButtonMessage(
                payload=payload
            )
            db.session.add(button_message)
            db.session.flush()
            db.session.commit()
            print("New buttonMessage added to database")

            try:
                message = Message(
                    from_id=from_id,
                    to_id=to_id,
                    text_message_id=None,
                    generic_message_id=None,
                    button_message_id=button_message.id
                )
                db.session.add(message)
                db.session.flush()
                db.session.commit()
                print("New message added to database")

                return None
            except:
                db.session.rollback()
                print("Unable to add message to database.")
                return None

        except:
            db.session.rollback()
            print("Unable to add buttonMessage to database.")
            return None

    @staticmethod
    def update_profile_field(user, field):
        user.last_profile_field_requested = field
        db.session.commit()
        return None

    @staticmethod
    def get_custom_message(key, user):
        language = user.locale if user else 'en'
        gender = user.gender if user else 'male'

        if not language:
            language = 'en'

        custom_messages = []
        if not gender:
            custom_messages = db.session.query(CustomMessage).filter(and_(CustomMessage.key == key,
                                                                          CustomMessage.language == language,
                                                                          CustomMessage.message is not None)).all()
        elif gender == 'male':
            custom_messages = db.session.query(CustomMessage).filter(and_(CustomMessage.key == key,
                                                                          CustomMessage.language == language,
                                                                          or_(CustomMessage.message is not None,
                                                                              CustomMessage.message_male is not None))).all()
        else:
            custom_messages = db.session.query(CustomMessage).filter(
                and_(CustomMessage.key == key,
                     CustomMessage.language == language,
                     or_(CustomMessage.message is not None,
                         CustomMessage.message_female is not None))).all()

        # custom_messages = db.session.query(CustomMessage).filter(and_(CustomMessage.key == key, CustomMessage.language == language)).all()
        if len(custom_messages) == 0:
            return None
        else:
            index = randint(0, len(custom_messages)-1)
            if gender == 'male':
                if custom_messages[index].message_male:
                    return custom_messages[index].message_male
                else:
                    return custom_messages[index].message
            elif gender == 'female':
                if custom_messages[index].message_female:
                    return custom_messages[index].message_female
                else:
                    return custom_messages[index].message
            else:
                return custom_messages[index].message

    @staticmethod
    def get_custom_messages():
        translations = db.session.query(CustomMessage).order_by(CustomMessage.key).all()
        return translations

    @staticmethod
    def delete_custom_messages():
        try:
            db.session.query(CustomMessage).delete()
            db.session.commit()
        except:
            db.session.rollback()

    @staticmethod
    def add_custom_message(key, language, message, message_female, message_male):

        if message_female == "":
            message_female = None

        if message_male == "":
            message_male = None

        try:
            custom_message = CustomMessage(
                key=key,
                language=language,
                message=message,
                message_female=message_female,
                message_male=message_male
            )
            db.session.add(custom_message)
            db.session.flush()
            db.session.commit()

            return None
        except:
            db.session.rollback()
            raise

    @staticmethod
    def has_ping_pong_access(user):
        return user.company_id == 1  # stefanini only
