from datetime import datetime

from sqlalchemy.dialects.postgresql import JSON

from application.db import db


class User(db.Model):
    __tablename__ = 'User'
    
    id = db.Column(db.Integer, primary_key=True)
    fb_id = db.Column(db.String())
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    gender = db.Column(db.String())
    profile_pic = db.Column(db.String())
    locale = db.Column(db.String())
    timezone = db.Column(db.String())
    created_at = db.Column(db.DateTime)
    last_activity_at = db.Column(db.DateTime)
    last_profile_field_requested = db.Column(db.String(), nullable=True)
    is_admin = db.Column(db.Boolean, default=False)
    is_active = db.Column(db.Boolean, default=False)
    company_id = db.Column(db.Integer, db.ForeignKey('Company.id', ondelete='cascade'), nullable=True)
    current_section = db.Column(db.Integer())
    section_status = db.Column(db.Integer())

    def __init__(self, fb_id, first_name, last_name, gender, profile_pic, locale, timezone):
        self.fb_id = fb_id
        self.first_name = first_name
        self.last_name = last_name
        self.gender = gender
        self.profile_pic = profile_pic
        self.locale = locale
        self.timezone = timezone
        self.created_at = datetime.utcnow()
        self.last_activity_at = datetime.utcnow()
        self.last_profile_field_requested = None
        self.current_section = 0
        self.section_status = 1

    def __repr__(self):
        return '<id {}>'.format(self.id)


class TextMessage(db.Model):
    __tablename__ = 'TextMessage'

    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String())

    def __init__(self, message):
        self.message = message

    def __repr__(self):
        return '<id {}>'.format(self.id)


class GenericMessage(db.Model):
    __tablename__ = 'GenericMessage'

    id = db.Column(db.Integer, primary_key=True)
    payload = db.Column(JSON)

    def __init__(self, payload):
        self.payload = payload

    def __repr__(self):
        return '<id {}>'.format(self.id)


class ButtonMessage(db.Model):
    __tablename__ = 'ButtonMessage'

    id = db.Column(db.Integer, primary_key=True)
    payload = db.Column(JSON)

    def __init__(self, payload):
        self.payload = payload

    def __repr__(self):
        return '<id {}>'.format(self.id)


class Message(db.Model):
    __tablename__ = 'Message'

    id = db.Column(db.Integer, primary_key=True)
    from_id = db.Column(db.Integer, db.ForeignKey('User.id', ondelete='cascade'), nullable=True)
    to_id = db.Column(db.Integer, db.ForeignKey('User.id', ondelete='cascade'), nullable=True)
    text_message_id = db.Column(db.Integer, db.ForeignKey('TextMessage.id', ondelete='cascade'), nullable=True)
    generic_message_id = db.Column(db.Integer, db.ForeignKey('GenericMessage.id', ondelete='cascade'), nullable=True)
    button_message_id = db.Column(db.Integer, db.ForeignKey('ButtonMessage.id', ondelete='cascade'), nullable=True)
    created_at = db.Column(db.DateTime)

    def __init__(self, from_id, to_id, text_message_id, generic_message_id, button_message_id):
        self.from_id = from_id
        self.to_id = to_id

        if text_message_id is not None:
            self.text_message_id = text_message_id

        if generic_message_id is not None:
            self.generic_message_id = generic_message_id

        if button_message_id is not None:
            self.button_message_id = button_message_id

        self.created_at = datetime.utcnow()

    def __repr__(self):
        return '<id {}>'.format(self.id)


class CustomMessage(db.Model):
    __tablename__ = 'CustomMessage'

    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String())
    language = db.Column(db.String())
    message = db.Column(db.String())
    message_male = db.Column(db.String())
    message_female = db.Column(db.String())

    def __init__(self, key, language, message, message_male, message_female):
        self.key = key
        self.language = language
        self.message = message
        self.message_male = message_male
        self.message_female = message_female

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return '<id {}>'.format(self.id)


class Company(db.Model):
    __tablename__ = 'Company'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    logo_url = db.Column(db.String())
    user_need_approval = db.Column(db.Boolean, default=False)

    def __init__(self, company_name):
        self.name = company_name

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return '<id {}>'.format(self.id)


class PingpongRequests(db.Model):
    __tablename__ = 'PingpongRequests'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('User.id', ondelete='cascade'), nullable=True)
    is_processed = db.Column(db.Boolean, default=False)
    is_looking_for_empty_room = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.DateTime)

    def __init__(self, user_id, is_processed, is_looking_for_empty_room):
        self.user_id = user_id
        self.is_processed = is_processed
        self.is_looking_for_empty_room = is_looking_for_empty_room
        self.created_at = datetime.utcnow()

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return '<id {}>'.format(self.id)


class PingpongRoom(db.Model):
    __tablename__ = 'PingpongRoom'

    id = db.Column(db.Integer, primary_key=True)
    is_empty_room = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime)

    def __init__(self, is_empty_room):
        self.is_empty_room = is_empty_room
        self.created_at = datetime.utcnow()

    def update(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return '<id {}>'.format(self.id)


