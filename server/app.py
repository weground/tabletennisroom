import getopt
import os
import sys
import json

from datetime import timedelta

from application.dashboard import app
from application.dashboard import ask
from application.dashboard.controllers import DashboardControllers

from application.fbmessenger.fbbot import FbBot
from application.fbmessenger.fbreceipt import FbReceipt
from application.fbmessenger.fbprocess import process_data

from flask_apscheduler import APScheduler

from flask import request, jsonify, make_response
from flask import send_from_directory
from rq import Queue
from worker import conn


@app.route('/static/sections/<path:filename>')
def serve_static_tutorial(filename):
    return send_from_directory(os.path.join('..', 'static', 'sections'), filename)


@app.route('/isMotion')
def ping_pong_is_motion():
    messaging_event = {'postback': {'payload': 'payload_ping_pong:toggle:0'}, 'sender': {'id': '1'}}

    q = Queue(connection=conn)
    q.enqueue(process_data, messaging_event, result_ttl=10)  # how long to hold on to the result of the job for 10 seconds

    return "ok", 200


@app.route('/isEmpty')
def is_empty():
    messaging_event = {'postback': {'payload': 'payload_ping_pong:toggle:1'}, 'sender': {'id': '1'}}

    q = Queue(connection=conn)
    q.enqueue(process_data, messaging_event, result_ttl=10)  # how long to hold on to the result of the job for 10 seconds

    return "ok", 200


@app.route('/', methods=['GET'])
def endpoint_get():
    if request.args.get("hub.mode") == "subscribe" and request.args.get("hub.challenge"):
        if not request.args.get("hub.verify_token") == os.environ["VERIFY_TOKEN"]:
            return "Verification token mismatch", 403
        return request.args["hub.challenge"], 200

    return "Urbs", 200


@app.route('/', methods=['POST'])
def endpoint():
    data = request.get_json()
    if data["object"] == "page":
        for entry in data["entry"]:
            for messaging_event in entry["messaging"]:

                q = Queue(connection=conn)
                q.enqueue(process_data, messaging_event, result_ttl=10)  # how long to hold on to the result of the job for 10 seconds

        return "ok", 200


def run_once(f):
    def wrapper(*args, **kwargs):
        if not wrapper.has_run:
            wrapper.has_run = True
            return f(*args, **kwargs)
    wrapper.has_run = False
    return wrapper


@run_once
def fb_initial_config():
    fb_bot = FbBot(os.environ["PAGE_ACCESS_TOKEN"], os.environ["PAGE_ID"])

    # set welcome screen
    fb_bot.configure_greeting_text("Hi, {{user_first_name}} Here you can have news about the availability of table tennis room.")
    fb_bot.configure_get_started_button()

    # set menu
    buttons = [
        {
            'type': 'postback',
            'title': "Sections",
            'payload': "tutorial"
        }
    ]
    fb_bot.send_persistant_menu(buttons)


def process_args(argv):

    print("start process_args")

    opts = None
    try:
        opts, args = getopt.getopt(argv, "j:", ["jobname="])
    except getopt.GetoptError:
        print('app.py -j <jobname>')

    if opts is not None:
        for opt, arg in opts:
            if opt == '-j':
                job_name = arg

                fb_receipt = FbReceipt()
                fb_receipt.execute_job(job_name)


if __name__ == '__main__':
    action = run_once(fb_initial_config)
    action()

    process_args(sys.argv[1:])

    app.run()
